# Network analysis with Cytoscape


## Part 1: Cytoscape UI
Here, you will get a short introduction on how to use Cytoscape to further analyse your gene expression data. You will learn how to use the software with two different examples:

* [RNA-Seq Data Network Analysis](https://cytoscape.org/cytoscape-tutorials/protocols/rna-seq-data-analysis/)
* [Basic Data Visualization](https://cytoscape.github.io/cytoscape-tutorials/protocols/basic-data-visualization/)

## Part 2: Cytoscape Automation
We will first go through some [slides](https://cytoscape.org/cytoscape-tutorials/presentations/intro-automation-2021-ismb.html#/5) to explain how automation works with Cytoscape. After that, you can start making your own R script showing how to connect your gene expression data analysis workflow to Cytoscape and automize your analysis steps:

* [Analysing top-regulated genes](https://cytoscape.org/cytoscape-tutorials/presentations/modules/RCy3_OmicsUseCase2/#/)
