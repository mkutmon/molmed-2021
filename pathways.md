# Pathway analysis

In this practical, you will learn the basic applications of pathway analysis by investigating a gene expression lung cancer dataset from TCGA (cancer-vs-healthy-tissue).


### Pathway analysis with PathVisio
You will learn how to perform pathway analysis using transcriptomics data in PathVisio. We will focus on data visualization, pathway statistics and biological interpretation. You can find the step-by-step instructions [here](https://mkutmon.gitlab.io/molmed-2021/pathway-analysis.html).

----

### Pathway analysis automation with R and Cytoscape
You will learn how you can perform pathway enrichment analysis in R and then visualize the data similarly as in PathVisio but automatically in Cytoscape.

You can find the Rmd file in Teams folder (Practicals / Day_5 / MolMed-PathwayAnalysis.Rmd)

----

Feel free to work alone or in pairs. Step-by-step instructions are provided. In between there are questions related to the methodology or biological interpretation. The aim of these questions is to make you really think about the analysis you are performing and the produced results.

You can downloaded the example lung cancer dataset here: [lung-cancer-data.txt](https://gitlab.com/mkutmon/molmed-wikipathways/raw/master/data/lung-cancer-data.txt).
