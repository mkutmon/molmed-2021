# Analyse PPI network from up-regulated genes

### Step 1: Gene selection
* Open the dataset (lung-cancer-data.xlsx) in Excel. 
* Select the significantly up-regulated genes with a log2FC > 1 → filter the table by log2FC (>1) and the adj.P.Value (< 0.05) (in Excel – Data tab – Filter Data!)
* Copy the gene names (2nd column) of the 383 genes.

### Step 2: Create PPI network for gene selection
* In Cytoscape, go to “File → New → Session...”
* Select the STRING protein query (drop-down box left to search field) and paste the 383 gene names in the query field 

![alt text](images/net2-f1.png)

* After clearing the search icon, the “Resolve Ambiguous Terms” dialog will pop up. Click on “Import” without any changes. 
* Cytoscape will create a network with 335 nodes and 3,794 edges

> **Q1: Network structure**	Are all nodes connected or do you see subnetworks or unconnected nodes?

* Select all nodes in the largest connect subnetwork (252 nodes) for the analysis (Shift and drag your mouse over all nodes). 
* File > New Network > From Selected Nodes All Edges. A new network will be created only containing the largest connected subnetwork with 252 nodes and 3,790 edges.

### Step 3: Investigate the network properties of the network

* Go to “Tools → Analyze Network”
* Treat network as undirected!
* In the Result tab (on the right side of the network), select "Analyzer" to show the results. 
* Click on "Show Node Degree Distribution"

> **Q2: Degree distribution** Is the network a scale-free network (most nodes with low degree and few nodes with high degree)?

Let's try to find the hub nodes (high degree) in the network.

* Go to the node table below the network and scroll all the way to the right. There you will find columns from the network analysis (Degree, Betweenness, etc.). 
* Click on the "Degree" column header to sort the nodes by their degree. 

> **Q3: Hub nodes:** Which proteins are the top most connected nodes (highest degree)?

> **Q4: Bottlenecks:** Nodes with a high betweenness are crucial for the connectivity of the network. Are the hub nodes also bottleneck nodes in the network or are there alternative routes through the network?

### Step 4: Find more information about major hub gene

Once you found an interesting hub gene, you can go and find out more information about the role of the protein within the context of the disease you are studying. The human protein atlas is an exceptional resource to find out more information about the expression of a gene/protein in different tissues, but it also provides detailed pathology analysis for relevant cancer genes. 

* Go to https://www.proteinatlas.org 
* Search for CDK1 hub gene of our previous analysis
* In the pathology tab, you can find information about the prognostic summary for different cancers, the RNA expression overview using data from TCGA, and show staining of the protein in different samples. 

> **Q5: Human protein atlas:** What kind of additional information can you find about CDK1 in relation to lung cancer (click on the Pathology tab for the gene entry)?
