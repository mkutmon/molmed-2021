# Installation instructions WikiPathways hands-on session

**Contact information:**
- In case you run into any problems during the installation of the required software, **do not hesitate to contact me**:
- Martina Summer-Kutmon (martina.kutmon (a) maastrichtuniversity.nl)

----

## Instructions to install PathVisio (for pathway analysis example – Part 1)
PathVisio is a commonly used pathway editor, analysis and visualization software that will be used during the pathway analysis hands-on session. 

----

#### Install PathVisio (Windows)
1. Download and install java 8 from [here](https://surfdrive.surf.nl/files/index.php/s/9pPP7KTNDpsjkBG)
2. Download the [pathvisio-3.3.0.zip](https://github.com/PathVisio/pathvisio/releases/download/v3.3.0/pathvisio_bin-3.3.0.zip) file and unzip it. 
3. Try to start PathVisio by double-clicking the pathvisio.bat (Windows Batch file). 

----

#### Install PathVisio (MacOSX / Linux)
1. Please make sure that you have Java 8 installed (OpenJDK is advised - info for [**MacOSX**](https://installvirtual.com/install-openjdk-8-on-mac-using-brew-adoptopenjdk/), info for [**Linux**](https://openjdk.java.net/install/))
2. Go to https://pathvisio.github.io/downloads and download the binary installation for PathVisio 3.3.0. Unzip the file.
3. Try to start PathVisio by running the pathvisio.jar (double-click) or pathvisio.sh (command-line) file.
4. On MacOS: If pathvisio.jar is saved as a Java Archive file, you need to use command-I to open Get Info window and change "Open with" to "Jar Launcher". 

----

#### Download required files
1. Download the identifier mapping database for Homo sapiens to enable automatic identifier mapping for gene products/proteins:<br/>https://bridgedb.github.io/data/gene_database/
2. Download the human pathway collection for Homo sapiens from WikiPathways:<br/>https://www.wikipathways.org/index.php/Download_Pathways

----

#### Additional information
- PathVisio website: www.pathvisio.org
- Latest PathVisio publication: "PathVisio 3: An Extendable Pathway Analysis Toolbox"
- Want to learn how to create your own pathway models? This will not be part of the hands-on session but you can check out the WikiPathways Academy, which will give you detailed instructions on how to get started: http://academy.wikipathways.org/

----

## Instructions to install Cytoscape (for automation example – Part 2)
Cytoscape is a widely adopted network analysis software that will be used during the network analysis hands-on session. Follow the instructions below to install Cytoscape:
 
1. Download the latest version of Cytoscape for your operating system:<br/>https://cytoscape.org/download.html
2. Install Cytoscape by going through the installation wizard. 
3. Try to start Cytoscape after the installation was successful.
4. In Cytoscape, go to Apps → App Manager. Search for WikiPathways and install the WikiPathways app.

#### Additional information
- Cytoscape website: https://cytoscape.org/
- Cytoscape tutorials: https://github.com/cytoscape/cytoscape-tutorials/wiki
- Cytoscape apps: http://apps.cytoscape.org/
