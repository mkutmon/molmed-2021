## WikiPathways practical
MolMed Course: Gene expression data analysis using R: How to make sense out of your RNA-Seq/microarray data

<img src="images/logos.png"  width="350">

---


*  **Contact information:** Martina Summer-Kutmon (martina.kutmon (a) maastrichtuniversity.nl)

Follow the installation instructions (available in the Practical folder in Teams) and make sure, you have downloaded **PathVisio**, the latest **WikiPathways human pathway collection** and the latest **BridgeDb human mapping** file.
Cytoscape is needed for the afternoon part of the practical.

---

### Morning - Pathway Analysis

After the introduction lecture on pathway analysis in the morning, you will learn how to use PathVisio and R to analyse and visualize gene expression data on biological pathways.
Follow instructions [here](pathways.md).

---

### Afternoon - Network Analysis

After the introduction lecture on network biology in the afternoon, you will learn how to use Cytoscape (UI and automation) to analyse gene expression data. 
Follow instructions [here](networks.md).
