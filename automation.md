# Pathway analysis in R

In PathVisio, you saw the basic functionality of the tool, which is also used to create the pathways in WikiPathways. 
Importantly, for reproducibility and efficiency, you can actually automize a lot of the features of pathway analysis in R. 

In the following R markdown file, we use rWikiPathways to retrieve pathways, clusterProfiler to perform pathway analysis and RCy3 to visualize the data on the pathway in Cytoscape. 

If you still have time (going through the complete example, will probably take about 20-30 min), feel free to try it out (otherwise, I will do a short demo at the end of the hands-on session): [PathwayAnalysis.Rmd](https://gitlab.com/mkutmon/molmed-wikipathways/-/raw/master/data/PathwayAnalysis.Rmd?inline=false) (right-click - save as - make sure .Rmd extension is used!).
