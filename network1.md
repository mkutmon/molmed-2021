## Example 1: Extend pathway with drug-target interaction data

Using the cell cycle pathway, you will learn how pathways can be used as a resource for network biology. Besides looking at the network structure, we will also add drug-target information to see which drugs in the DrugBank database are known to target genes in the cell cycle pathway.

Start by opening Cytoscape!

* First, you need to install the WikiPathways and CyTargetLinker apps. Go to Apps > App Manager > Search for WikiPathways / CyTargetLinker and install both apps. 
* In the search bar in the control panel on the left, select WikiPathways in the drop-down and search for “cell cycle” (with quotes).

![alt text|small](images/net1-f1.png)

* Select the Cell Cycle pathway (WP179) and click “**Import as Network**”!

![alt text](images/net1-f2.png)

* You should now see a network with 164 nodes and 223 edges.

>  **Question 1: Network structure**<br/>Are all nodes connected or do you see subnetworks or unconnected nodes? Make a screenshot.

Now we are going to investigate which drugs are known to target the proteins in this pathway.

* Go to Apps > CyTargetLinker > Extend network
* Fill in the following settings (select the _linkset_ directory in the NetworkAnalysis folder that you downloaded before when clicking on browse): 

![alt text](images/net1-f3.png)

* Now the known drugs from DrugBank have been added – information about drug name, drug category or approval status can be found in the node table. Go to the Styles Tab on the left side and change the label mapping column to "name" so names of both genes and drugs are displayed in the network.

![alt text](images/net1-f4.png)

* If you go to “Apps → CyTargetLinker → Show Result Panel”, you can see how many drugs were added. You can find more information about the drugs on the DrugBank website (www.drugbank.ca) to investigate if any of the drugs are used for lung cancer treatment. The identifiers and names of the drugs can be found in the node table below the network. 

>  **Question 2: Network extension**<br/>How many drugs were added to the network? Which genes are targeted by many drugs?

* To import the gene expression data, go to File > Import > Table from File. Select the lung-cancer-data.xlsx file in the NetworkAnalysis folder. USe the following settings:

![alt text](images/net1-f5.png)

* Now we need to change the node fill color for the nodes to show the log2FC. Go to the Styles tab and change the mapping of the Fill Color. Use the log2FC column and choose a continous mapping. Usually the default gradient should be okay.

![alt text](images/net1-f6.png)

>  **Question 3: Drug targets**<br/>Are any of the highly targeted proteins in the pathway differentially expressed in lung cancer?


## Automation script:
You can find the R script to perform this analysis here: [NetworkAnalysis1.Rmd](https://gitlab.com/mkutmon/molmed-wikipathways/-/raw/master/data/NetworkAnalysis1.Rmd?inline=false) (right-click - save as - make sure .Rmd extension is used!).
